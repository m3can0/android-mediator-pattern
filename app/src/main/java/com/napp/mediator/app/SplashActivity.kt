package com.napp.mediator.app

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import com.napp.mediator.app.databinding.ActivitySplashBinding
import com.napp.mediator.loginlib.ViewBindingActivity
import com.napp.mediator.loginlib.startLoginActivity

class SplashActivity : ViewBindingActivity<ActivitySplashBinding>() {
    override fun onInflateViewBinding(layoutInflater: LayoutInflater) = ActivitySplashBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO replace with not deprecated method...
        Handler().postDelayed({
              this.startLoginActivity(Mediator())
        }, 2000)
    }
}