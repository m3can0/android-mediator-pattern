package com.napp.mediator.app.data.dto

import com.google.gson.annotations.SerializedName
import com.napp.mediator.loginlib.data.entity.Comment

data class CommentDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("body")
    val body: String
) {
    fun mapToComment() : Comment{
        return Comment(
            id = id,
            content = title + body
        )
    }
}