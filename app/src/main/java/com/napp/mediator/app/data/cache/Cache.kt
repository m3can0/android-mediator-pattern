package com.napp.mediator.app.data.cache

object SingletonRamCache: ICache {

    private val cacheMap = hashMapOf<String, Any>()

    override fun containsKeys(key: String): Boolean {
        return cacheMap.containsKey(key)
    }

    override fun <V> get(key: String): V? {
        return cacheMap[key] as? V
    }

    override fun <V> add(key: String, value: V) {
        cacheMap[key] = value as Any
    }

    override fun remove(key: String) {
        cacheMap.remove(key)
    }

    override fun clear() {
        cacheMap.clear()
    }

}

interface ICache {
    fun containsKeys(key: String) : Boolean
    fun <V> get(key: String) : V?
    fun <V> add(key: String, value: V)
    fun remove(key: String)
    fun clear()
}