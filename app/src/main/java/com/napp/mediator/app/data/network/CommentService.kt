package com.napp.mediator.app.data.network

import com.napp.mediator.app.data.dto.CommentDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CommentService {
    @GET("posts/{postId}/comments")
    fun commentsList(@Path("postId") postId: Int): Call<List<CommentDto>>
}