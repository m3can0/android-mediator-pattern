package com.napp.mediator.app.data.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitNetworkManager private constructor() : INetworkManager {

    companion object {
        private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

        val instance: RetrofitNetworkManager by lazy {
            RetrofitNetworkManager()
        }
    }

    private val retrofit : Retrofit = Retrofit.Builder().apply {
        baseUrl(BASE_URL)
        addConverterFactory(GsonConverterFactory.create())
    }.build()

    override suspend fun <S, R> callRequest(serviceClass: Class<S>, request: (service: S) -> Call<R>): R {
        val service = retrofit.create(serviceClass)
        val call = request.invoke(service)
        return call.enqueueSuspendCoroutine()
    }
}

interface INetworkManager {
    suspend fun <S, R> callRequest(serviceClass: Class<S>, request: (service: S) -> Call<R>): R
}