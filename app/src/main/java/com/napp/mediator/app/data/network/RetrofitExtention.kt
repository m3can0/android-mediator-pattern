package com.napp.mediator.app.data.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun <T> Call<T>.enqueueSuspendCoroutine(): T {
    return suspendCoroutine { cont ->
        enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, error: Throwable) {
                cont.resumeWithException(error)
            }

            override fun onResponse(
                call: Call<T>?,
                response: Response<T>
            ) {
                when {
                    response.isSuccessful && response.body() != null -> cont.resume(response.body()!!)
                    response.isSuccessful && response.body() == null -> cont.resumeWithException(
                        Exception("Api call successful but Null body")
                    )
                    else -> {
                        val errorCode = response.code()
                        val errorBody = response.errorBody().toString()
                        cont.resumeWithException(Exception("$errorCode: $errorBody"))
                    }
                }
            }
        })
    }
}