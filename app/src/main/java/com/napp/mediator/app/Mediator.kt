package com.napp.mediator.app

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.napp.mediator.app.data.LocalDataManager
import com.napp.mediator.app.data.network.CommentService
import com.napp.mediator.app.data.network.RetrofitNetworkManager
import com.napp.mediator.loginlib.IMediator
import com.napp.mediator.loginlib.data.entity.Comment

class Mediator : IMediator {
    override fun startMainActivity(currentActivity: AppCompatActivity) {
        currentActivity.startMainActivity()
    }

    override fun getIsChecked(appContext: Context): Boolean {
        return LocalDataManager(appContext).isKeepMeLoginEnabled
    }

    override fun setIsChecked(appContext: Context, isKeepMeLoginEnabled: Boolean) {
        LocalDataManager(appContext).isKeepMeLoginEnabled = isKeepMeLoginEnabled
    }

    override suspend fun getComments():List<Comment> {
        val commentDtoList = RetrofitNetworkManager.instance.callRequest(CommentService::class.java) { service ->
            service.commentsList(1)
        }
        return commentDtoList.map { dto -> dto.mapToComment() }
    }
}