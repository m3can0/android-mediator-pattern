package com.napp.mediator.app

import android.content.Intent
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.napp.mediator.app.databinding.ActivityMainBinding
import com.napp.mediator.loginlib.ViewBindingActivity

class MainActivity : ViewBindingActivity<ActivityMainBinding>() {
    override fun onInflateViewBinding(layoutInflater: LayoutInflater) = ActivityMainBinding.inflate(layoutInflater)
}

fun AppCompatActivity.startMainActivity() {
    startActivity(Intent(this, MainActivity::class.java))
}