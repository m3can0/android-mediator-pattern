package com.napp.mediator.app.data.cache

import java.util.concurrent.TimeUnit

// I don't have any merit here thanks to kezhenxu94 from medium.

class ExpirableCacheDecorator(
    override val cache: ICache,
    var millisFlushInterval: Long = TimeUnit.MINUTES.toMillis(5) // 5 min ttl
): ICacheDecorator {

    private val nanoFlushInterval: Long
        get() = TimeUnit.MILLISECONDS.toNanos(millisFlushInterval)
    private var nanoLastFlush: Long = System.nanoTime()

    override fun containsKeys(key: String): Boolean = cache.containsKeys(key)

    override fun <V> get(key: String): V? {
        recycle()
        return cache.get<V>(key)
    }

    override fun <V> add(key: String, value: V) {
        recycle()
        cache.add(key, value)
    }

    override fun remove(key: String) {
        cache.remove(key)
        recycle()
    }

    override fun clear() {
        cache.clear()
    }

    private fun recycle() {
        val shouldClearCache = System.nanoTime() - nanoLastFlush >= nanoFlushInterval
        if(shouldClearCache) {
            clear()
        }
    }
}

interface ICacheDecorator: ICache {
    val cache: ICache
}