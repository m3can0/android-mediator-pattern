package com.napp.mediator.app.data.network

import com.napp.mediator.app.data.cache.ICache
import retrofit2.Call

class CacheNetworkDecorator(
    override val manager: INetworkManager,
    private val cache: ICache
    ) : INetworkDecorator {

    override suspend fun <S, R> callRequest(
        serviceClass: Class<S>,
        request: (service: S) -> Call<R>
    ): R {
        // FIXME This is only to share the pattern.
        //  For real implementation it's too limited
        //  if you have multiple call with the same service
        //  Solution: Add Request object with single name for each method

        val serviceKey = serviceClass.simpleName
        if(cache.containsKeys(serviceKey) ) {
            cache.get<R>(serviceKey)?.let { return it }
        }

        // If we did not save the answer, then
        val response = manager.callRequest(serviceClass, request)
        cache.add(serviceKey, response)
        return response
    }

    fun <S> removeCachedResponse(serviceClass: Class<S>) {
        cache.remove(serviceClass.simpleName)
    }

    fun clearCachedResponse() {
        cache.clear()
    }
}

interface INetworkDecorator : INetworkManager {
    val manager: INetworkManager
}