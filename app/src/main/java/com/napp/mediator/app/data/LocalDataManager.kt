package com.napp.mediator.app.data

import android.content.Context
import android.content.SharedPreferences

class LocalDataManager(appContext: Context) : ILocalDataManager {

    companion object {
        private const val PREFERENCE = "MEDIATOR_PREF"
        private const val KEY_IS_KEEP_ME_LOGIN_ENABLED = "isKeepMeLoginEnabled"
    }

    private val sharedPreferences: SharedPreferences = appContext.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE)

    override var isKeepMeLoginEnabled: Boolean
        get() = sharedPreferences.getBoolean(KEY_IS_KEEP_ME_LOGIN_ENABLED, false)
        set(value) {
            sharedPreferences.edit().apply {
                putBoolean(KEY_IS_KEEP_ME_LOGIN_ENABLED, value)
                apply()
            }
        }
}

interface ILocalDataManager {
    var isKeepMeLoginEnabled: Boolean
}