package com.napp.mediator.loginlib.data.entity

data class Comment(
    val id: Int,
    val content: String
)