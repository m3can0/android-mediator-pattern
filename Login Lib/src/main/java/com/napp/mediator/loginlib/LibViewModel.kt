package com.napp.mediator.loginlib

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.napp.mediator.loginlib.data.entity.Comment
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class LibViewModel(
    private val mediator: IMediator,
    private val backgroundThread: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _networkResult = MutableLiveData<CharSequence>()
    val networkResult: LiveData<CharSequence> = _networkResult

    fun fetchCommentList() {
        viewModelScope.launch(Dispatchers.Main) {
            _networkResult.value = try {
                val commitList = withContext(backgroundThread) { mediator.getComments() }
                Log.i("ViewModel", "commentList: $networkResult")
                convertCommentList(commitList)
            } catch (exception: Exception) {
                convertException(exception)
            }
        }
    }

    private fun convertCommentList(commentList: List<Comment>): CharSequence {
        var formattedText: CharSequence = ""
        commentList.forEach { comment ->
            formattedText = "$formattedText${comment.id}\n"
        }
        return formattedText
    }

    private fun convertException(exception: Exception): CharSequence {
        val exceptionText = exception.toString()
        return SpannableString(exceptionText).apply {
            setSpan(
                ForegroundColorSpan(Color.RED),
                0, exceptionText.length - 1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}

fun AppCompatActivity.createLoginViewModel(mediator: IMediator): LibViewModel {
    return ViewModelProvider(this, object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return LibViewModel(mediator) as T
        }
    }).get(LibViewModel::class.java)
}