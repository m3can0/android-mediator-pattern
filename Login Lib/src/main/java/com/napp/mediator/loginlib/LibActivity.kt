package com.napp.mediator.loginlib

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.napp.mediator.loginlib.databinding.ActivityLibBinding

class LibActivity : ViewBindingActivity<ActivityLibBinding>() {

    private val activityMediator: IMediator by lazy {
        intent.getSerializableExtra(ARG_ACTIVITY_MEDIATOR) as IMediator
    }

    private val viewModel : LibViewModel by lazy {
        createLoginViewModel(activityMediator)
    }

    override fun onInflateViewBinding(layoutInflater: LayoutInflater) = ActivityLibBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.startActivityBtn.setOnClickListener {
            activityMediator.startMainActivity(this)
        }

        setCheckboxExample()

        viewModel.fetchCommentList()
        viewModel.networkResult.observe(this) {
            binding.networkResultTv.text = it
        }
    }

    private fun setCheckboxExample() = with(binding.isCheckedCb) {
        isChecked = activityMediator.getIsChecked(applicationContext)
        setOnClickListener {
            activityMediator.setIsChecked(applicationContext, isChecked)
        }
    }
}

private const val ARG_ACTIVITY_MEDIATOR = "activityMediator"

fun AppCompatActivity.startLoginActivity(mediator: IMediator) {
    startActivity(Intent(this, LibActivity::class.java).apply {
        putExtra(ARG_ACTIVITY_MEDIATOR, mediator)
    })
}