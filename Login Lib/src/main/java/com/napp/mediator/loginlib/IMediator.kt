package com.napp.mediator.loginlib

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.napp.mediator.loginlib.data.entity.Comment
import java.io.Serializable

interface IMediator : Serializable {
    fun startMainActivity(currentActivity: AppCompatActivity)
    fun getIsChecked(appContext: Context) : Boolean
    fun setIsChecked(appContext: Context, isKeepMeLoginEnabled : Boolean)
    suspend fun getComments(): List<Comment>
}